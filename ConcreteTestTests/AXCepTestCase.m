//
//  AXCepTestCase.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/30/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AXCep.h"

@interface AXCepTestCase : XCTestCase
@property (nonatomic, strong) NSDictionary *json;
@end

@implementation AXCepTestCase

- (void)setUp
{
    [super setUp];
    
    self.json = @{ @"cep": @"14051010",
                   @"tipoDeLogradouro": @"Rua",
                   @"logradouro": @"Manoel Duarte Ortigoso",
                   @"bairro": @"Jardim Antártica",
                   @"cidade": @"Ribeirão Preto",
                   @"estado": @"SP"};
}

- (void)tearDown
{
    self.json = nil;
    [super tearDown];
}

- (void)testCepCreationFromJson
{
    NSError *error = nil;
    AXCep *cep = [[AXCep alloc] initWithJsonDictionary:self.json error:&error];
    XCTAssertNil(error, @"Should not return error to create cep");
    XCTAssertNotNil(cep, @"Should return a valid AXCep object");
    XCTAssertEqualObjects(cep.cep, self.json[@"cep"], @"Cep data should be the same");
    XCTAssertEqualObjects(cep.tipo, self.json[@"tipoDeLogradouro"], @"Cep data should be the same");
    XCTAssertEqualObjects(cep.logradouro, self.json[@"logradouro"], @"Cep data should be the same");
    XCTAssertEqualObjects(cep.bairro, self.json[@"bairro"], @"Cep data should be the same");
    XCTAssertEqualObjects(cep.cidade, self.json[@"cidade"], @"Cep data should be the same");
    XCTAssertEqualObjects(cep.estado, self.json[@"estado"], @"Cep data should be the same");
}

- (void)testProperFetchOfSavedCep
{
    AXCep *cep1 = [[AXCep alloc] initWithJsonDictionary:self.json error:nil];
    AXCep *cep2 = [AXCep fetchCep:cep1.cep];
    XCTAssertNotNil(cep2, @"Should return a valid AXCep object");
    XCTAssertEqualObjects(cep1.cep, cep2.cep, @"Cep data should be the same");
    XCTAssertEqualObjects(cep1.tipo, cep2.tipo, @"Cep data should be the same");
    XCTAssertEqualObjects(cep1.logradouro, cep2.logradouro, @"Cep data should be the same");
    XCTAssertEqualObjects(cep1.bairro, cep2.bairro, @"Cep data should be the same");
    XCTAssertEqualObjects(cep1.cidade, cep2.cidade, @"Cep data should be the same");
    XCTAssertEqualObjects(cep1.estado, cep2.estado, @"Cep data should be the same");
    XCTAssertEqualObjects(cep1.searchDate.shortFormat, cep2.searchDate.shortFormat, @"Cep data should be the same");
}

- (void)testPersistenceCleanup
{
    NSError *error = [AXCep removeAllSavedObjects];
    XCTAssertNil(error, @"Should not return error on data deletion");
    NSArray *objects = [AXCep savedObjects];
    XCTAssertEqual(objects.count, 0, @"Array of saved objects must be empty after deletion");
}

- (void)testPersistence
{
    [AXCep removeAllSavedObjects];
    
    NSDictionary *json2 = @{ @"cep": @"14025530",
                             @"tipoDeLogradouro": @"Rua",
                             @"logradouro": @"Professor Alonso Ferraz",
                             @"bairro": @"Alto da Boa Vista",
                             @"cidade": @"Ribeirão Preto",
                             @"estado": @"SP"};
    
    NSDictionary *json3 = @{ @"cep": @"14025540",
                             @"tipoDeLogradouro": @"Rua",
                             @"logradouro": @"Mariano Pedroso de Almeida",
                             @"bairro": @"Alto da Boa Vista",
                             @"cidade": @"Ribeirão Preto",
                             @"estado": @"SP"};
    
    AXCep *cep = [[AXCep alloc] initWithJsonDictionary:self.json error:nil];
    cep = [[AXCep alloc] initWithJsonDictionary:json2 error:nil];
    cep = [[AXCep alloc] initWithJsonDictionary:json3 error:nil];
    
    NSInteger count = [[AXCep savedObjects] count];
    
    XCTAssertEqual(count, 3, @"After 3 cep object creations, saved objects must return 3");
}

@end
