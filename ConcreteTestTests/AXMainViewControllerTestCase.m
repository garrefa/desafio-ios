//
//  AXMainViewControllerTestCase.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 10/1/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Specta/Specta.h>
#import <OCMock/OCMock.h>
#define EXP_SHORTHAND
#import <Expecta/Expecta.h>

#import "AXMainViewController.h"
#import "AXCEPField.h"
#import "AXCepSearch.h"

SpecBegin(AXMainViewController)

describe(@"AXMainViewController", ^{

    __block AXMainViewController *_vc;
    
    beforeEach(^{
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"iPhoneStoryboard" bundle:nil];
        UINavigationController *nav = [mainStoryboard instantiateInitialViewController];
        _vc = (AXMainViewController *)[nav visibleViewController];
        
        UIView *view = _vc.view;
        expect(view).toNot.beNil();
    });
    
    it(@"should be instantiated from the storyboard", ^{
        expect(_vc).toNot.beNil();
        expect(_vc).to.beInstanceOf([AXMainViewController class]);
    });
    
    it(@"should have an outlet for AXCEPField", ^{
        expect(_vc.cepTextField).toNot.beNil();
        expect(_vc.cepTextField).to.beInstanceOf([AXCEPField class]);
    });
    
    it(@"should have an outlet for UILabel cepInfoLabel", ^{
        expect(_vc.cepInfoLabel).toNot.beNil();
        expect(_vc.cepInfoLabel).to.beInstanceOf([UILabel class]);
        expect(_vc.cepInfoLabel.text).to.beNil();
    });
    
    it(@"should have an outlet for UIView internetErrorView", ^{
        expect(_vc.internetErrorView).toNot.beNil();
        expect(_vc.internetErrorView).to.beInstanceOf([UIView class]);
    });
    
});

SpecEnd