//
//  AXSDKTestClient.h
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/26/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface AXAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end