//
//  NSDate+Additions.h
//  SDKTests
//
//  Created by Alexandre Garrefa on 10/1/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Additions)
- (NSString *)formatedWithFormat:(NSString *)format;
- (NSString *)shortFormat;
@end
