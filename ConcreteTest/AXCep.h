//
//  AXCep.h
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/26/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <Mantle.h>

@interface AXCep : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *cep;
@property (nonatomic, strong) NSString *tipo;
@property (nonatomic, strong) NSString *logradouro;
@property (nonatomic, strong) NSString *bairro;
@property (nonatomic, strong) NSString *cidade;
@property (nonatomic, strong) NSString *estado;
@property (nonatomic, copy, readonly) NSDate *searchDate;

- (instancetype)initWithJsonDictionary:(NSDictionary *)json error:(NSError **)error;

- (NSAttributedString *)attributedDescription;
+ (AXCep *)fetchCep:(NSString *)cep;
+ (NSArray *)savedObjects;
+ (NSError *)removeAllSavedObjects;

@end
