//
//  AXCEPTableViewCell.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/30/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXCEPTableViewCell.h"

@implementation AXCEPTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
