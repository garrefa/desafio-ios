//
//  AXSearchsTableViewController.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/27/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXSearchsTableViewController.h"
#import "AXCep.h"
#import "AXCEPTableViewCell.h"

@interface AXSearchsTableViewController ()
@property (nonatomic, strong) NSArray *ceps;

@end

@implementation AXSearchsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.ceps = [AXCep savedObjects];
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ceps.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXCEPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CEPCELL" forIndexPath:indexPath];

    NSString *cep = [self.ceps objectAtIndex:indexPath.row];
    cell.cepDetailsLabel.attributedText = [[AXCep fetchCep:cep] attributedDescription];
    return cell;
}

@end
