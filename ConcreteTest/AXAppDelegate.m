//
//  AXAppDelegate.m
//  ConcreteTest
//
//  Created by Alexandre Garrefa on 10/1/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXAppDelegate.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation AXAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setNavigationBarAppearance];
    
    [self setReachability];
    
    return YES;
}

#pragma mark -
#pragma mark - Private Methods

- (void)setNavigationBarAppearance
{
    UINavigationBar *proxy = [UINavigationBar appearance];
    proxy.tintColor = [UIColor purpleColor];
    [proxy setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor purpleColor],
                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:18.0f]}];
}

- (void)setReachability
{
    NSURL *baseURL = [NSURL URLWithString:@"http://google.com/"];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    [manager.reachabilityManager startMonitoring];
}

@end
