//
//  AXCEPField.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/29/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXCEPField.h"

@interface AXCEPField () <UITextFieldDelegate>
@property (nonatomic, strong) NSNumberFormatter *formatter;
@end

@implementation AXCEPField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.delegate = self;
}

#pragma mark -
#pragma mark - TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger tf_length = [textField.text length];
    NSUInteger maxLength = 9;
    NSUInteger firstPartLength = 5;
    NSString *validChars = @"01234567890";
    
    // Deleting chars
    if (string.length == 0) {
        // Deny deleting chars that are not the last char
        if (range.location + range.length != tf_length) return NO;
        // Allow all other deletions
        if (tf_length == firstPartLength+2) {
            textField.text = [textField.text substringToIndex:firstPartLength];
            return NO;
        }
        return YES;
    }
    
    // Deny non numbers
    if ([validChars rangeOfString:string].location == NSNotFound) return NO;
    
    // Deny more than maxsize chars
    if (tf_length >= maxLength) return NO;
    
    // If end of first part, add separator
    if (tf_length == firstPartLength && string.length == 1) {
        textField.text = [textField.text stringByAppendingFormat:@"-%@",string];
        return NO;
    }
    
    // Allow valid aditions
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.cepDelegate ax_cepFieldDidBeginEditing:textField];
}

@end
