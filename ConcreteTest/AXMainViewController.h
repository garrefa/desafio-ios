//
//  AXMainViewController.h
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/27/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AXCEPField;
@interface AXMainViewController : UIViewController
@property (weak, nonatomic) IBOutlet AXCEPField *cepTextField;
@property (weak, nonatomic) IBOutlet UILabel *cepInfoLabel;
@property (weak, nonatomic) IBOutlet UIView *internetErrorView;
@end
