//
//  AXMainViewController.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/27/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXMainViewController.h"
#import "AXCep.h"
#import "AXCepSearch.h"
#import "AXCEPField.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "UIActivityIndicatorView+AFNetworking.h"

@interface AXMainViewController () <UITextFieldDelegate, AXCEPFieldDelegate>
@property (strong, nonatomic) UIView *inputAccView;
@property (strong, nonatomic) UIActivityIndicatorView *loading;
@end

@implementation AXMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.cepTextField.cepDelegate = self;
    self.title = @"Consulta CEP";
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusNotReachable) [self.internetErrorView setHidden:NO];
        else [self.internetErrorView setHidden:YES];
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.loading];
    
    self.cepInfoLabel.attributedText = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

#pragma mark -
#pragma mark - Private Methods

-(void)createInputAccessoryView
{
    static dispatch_once_t iav_onceToken;
    dispatch_once(&iav_onceToken, ^{
        UIView *inputAccView = [[UIView alloc] initWithFrame:CGRectMake(00.0, 0.0, 320.0, 40.0)];
        [inputAccView setBackgroundColor:[UIColor grayColor]];
        self.inputAccView = inputAccView;
        
        UIButton *goBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [goBtn setTitle: @"Buscar" forState: UIControlStateNormal];
        [goBtn setBackgroundColor: [UIColor blueColor]];
        [goBtn addTarget:self action:@selector(cepSearch) forControlEvents:UIControlEventTouchUpInside];
        
        [self.inputAccView addSubview:goBtn];
        
        [goBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(inputAccView.mas_top).with.offset(2);
            make.right.equalTo(inputAccView.mas_right).with.offset(-10);
            make.bottom.equalTo(inputAccView.mas_bottom).with.offset(-2);
            make.width.equalTo(@80);
            make.height.equalTo(inputAccView.mas_height).with.offset(-4);
        }];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setTitle: @"Cancelar" forState: UIControlStateNormal];
        [cancelBtn setBackgroundColor: [UIColor redColor]];
        [cancelBtn addTarget:self action:@selector(dismissKeyboard) forControlEvents:UIControlEventTouchUpInside];
        
        [self.inputAccView addSubview:cancelBtn];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(inputAccView.mas_top).with.offset(2);
            make.left.equalTo(inputAccView.mas_left).with.offset(10);
            make.bottom.equalTo(inputAccView.mas_bottom).with.offset(-2);
            make.width.equalTo(@80);
            make.height.equalTo(inputAccView.mas_height).with.offset(-4);
        }];
    });
}

- (void)cepSearch
{
    [self dismissKeyboard];
    NSString *cepToSearch = [[self.cepTextField text] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    if (cepToSearch.length != 8) {
        [SVProgressHUD showErrorWithStatus:@"CEP inválido."];
        return;
    }
    
    AXCep *cep = [AXCep fetchCep:cepToSearch];
    if(cep) self.cepInfoLabel.attributedText = [cep attributedDescription];
    else {
        __weak typeof(self) weakSelf = self;
        
        NSURLSessionDataTask *task =
        [AXCepSearch searchForCep:cepToSearch withSuccessBlock:^(AXCep *cep, NSError *error) {
            if(cep) weakSelf.cepInfoLabel.attributedText = [cep attributedDescription];
        } andFailureBlock:^(NSURLSessionDataTask *task, NSError *error) {
            
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            NSInteger statuscode = response.statusCode;
            
            if (statuscode == 404) {
                [SVProgressHUD showErrorWithStatus:@"CEP não encontrado."];
            }
            else {
                [SVProgressHUD showErrorWithStatus:@"Serviço indisponível."];
            }
        }];
        
        [self.loading setAnimatingWithStateOfTask:task];
        
    }
}

- (void)dismissKeyboard
{
    [self.cepTextField resignFirstResponder];
}

- (UIActivityIndicatorView *)loading
{
    static dispatch_once_t load_onceToken;
    dispatch_once(&load_onceToken, ^{
        _loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _loading.hidesWhenStopped = YES;
    });
    
    return _loading;
}

#pragma mark -
#pragma mark - AXCepField Delegate

- (void)ax_cepFieldDidBeginEditing:(UITextField *)textField
{
    [self createInputAccessoryView];
    
    [textField setInputAccessoryView:self.inputAccView];
}

@end