//
//  AXCepSearch.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/27/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXCepSearch.h"
#import "AXCep.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation AXCepSearch

+ (NSURLSessionDataTask *)searchForCep:(NSString *)cep withSuccessBlock:(void (^)(AXCep *cep, NSError *error))success
                       andFailureBlock:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    return [[AXAPIClient sharedClient] GET:[NSString stringWithFormat:@"/cep/%@",cep] parameters:nil
                                   success:^(NSURLSessionDataTask *task, id responseObject)
            {
                NSError *error = nil;
                
                AXCep *cep = [[AXCep alloc]initWithJsonDictionary:responseObject error:&error];
                
                if(success) {
                    if (error) success(nil,error);
                    else success(cep,nil);
                }
                
            }
                                   failure:^(NSURLSessionDataTask *task, NSError *error)
            {
                NSLog(@"error: %@\n\n%@",task.originalRequest.URL,error.localizedDescription);
                if(failure) failure(task, error);
            }];
}

@end
