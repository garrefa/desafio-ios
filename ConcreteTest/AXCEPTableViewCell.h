//
//  AXCEPTableViewCell.h
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/30/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXCEPTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cepDetailsLabel;

@end
