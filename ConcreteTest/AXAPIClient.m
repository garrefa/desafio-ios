//
//  AXSDKTestClient.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/26/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXAPIClient.h"

static NSString * const kAXAPIClientBaseURLString = @"http://correiosapi.apphb.com/";

@implementation AXAPIClient

+ (instancetype)sharedClient
{
    static AXAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[AXAPIClient alloc] initWithBaseURL:[NSURL URLWithString:kAXAPIClientBaseURLString]];
    });
    
    return _sharedClient;
}

@end
